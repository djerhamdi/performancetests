#!/bin/sh
rm ./sorttest.txt
cmake . && make

sudo nvidia-smi -pm 1


for j in 1 2 3 4 5
do 
    echo "executing measurement No $j"
    for i in  1 10 50 100 200 300 400 500 600 700 800 900 1000 3000 5000 7000 10000 50000 100000 300000 500000 700000 1000000 5000000 10000000 30000000 50000000 70000000 100000000
    do
    echo " measuring with $i elements"
    ./bin/sorttest $i >> sorttest.txt
    done
 done

sudo nvidia-smi -pm 0

git add sorttest.txt
git commit -m " measurement taken $(date)"
git push origin master
echo "All done, file sent !"