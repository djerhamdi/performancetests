#!/bin/sh
rm ./sortMeasurements.txt
rm ./computeMeasurements.txt
rm ./copyMeasurementWithWarmUp.txt
cmake . && make

# run tests
echo "executing tests"
sudo nvidia-smi -pm 1
./bin/test_reduceVScalculation
./bin/test_stdSortVSthrustSort
##copytime doesn't have test
echo "tests done"
for j in 1 2 3 4 5 6 7 8 9 10
do
    echo "executing measurement No $j"
    # launch copy time with 10m elements
    ./bin/copyTime 10000000 >>copyMeasurementWithWarmUp.txt
    
     for i in  1 10 50 100 200 300 400 500 600 700 800 900 1000 3000 5000 7000 10000 50000 100000 300000 500000 700000 1000000 5000000 10000000 30000000 50000000 70000000 100000000
    do
        echo " measuring with $i elements"
        ./bin/stdSortVSthrustSort $i >>sortMeasurements.txt
        ./bin/reduceVScalculation $i >>computeMeasurements.txt
    done
done

sudo nvidia-smi -pm 0

echo "launching warm up measurement without presistence mode "
sh ./presistenceTest.sh

echo "done measuring"


echo "pushing measurements back to repo"
git add sortMeasurements.txt computeMeasurements.txt copyMeasurementWithWarmUp.txt copyMeasurementWithoutWarmUp.txt
git commit -m " measurement taken $(date)"
git push origin master
echo "All done, file sent !"


