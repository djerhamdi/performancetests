#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/sort.h>
#include <iostream>
#include <chrono>

std::chrono::time_point<std::chrono::steady_clock> get_time (){

	std::chrono::time_point<std::chrono::steady_clock> tick = std::chrono::steady_clock::now();
	return tick;		
}


int main (int argc, char const* argv[]) {

    if (argc < 2){
          std::cout << "Please specify dimension as a command line argument " << std::endl;
          exit (-1);
    }
    int dimension = atoi(argv[1]) ; 

    // chrono parameters
    double ms;
    std::chrono::duration <double> duration;
    std::chrono::time_point<std::chrono::steady_clock> tic,toc;
    
    // Measure warm up
    cudaEvent_t start,stop, tick, tock;
    tic = get_time();
    cudaEventCreate(&start);
    toc = get_time();
    
    // cudaEvent parameters
    cudaEventCreate(&stop);
    cudaEventCreate(&tick);
    cudaEventCreate(&tock);
    float H2Dtime = 0 ,D2Htime = 0, fullMeasure = 0, sortTime = 0, memAlloc = 0; 
    

    // Display time
    duration = toc - tic; 
    ms = duration.count() * 1000.0f;
    std::cout << "Warm-up =\t" << ms << "\tms" << std::endl;
    
    thrust::host_vector <int> h_vec (dimension);
    thrust::host_vector <int> h_vec_2 (dimension);
    thrust::generate(h_vec.begin(), h_vec.end(), rand);

    // malloc
    cudaEventRecord(start);
    thrust::device_vector <int> d_vec (dimension);
    cudaDeviceSynchronize();
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&memAlloc, start, stop);
    std::cout <<"Device malloc =\t" << memAlloc <<"\tms" <<std::endl;
    
    cudaEventRecord(tick);
    //H2D
    cudaEventRecord(start);
    d_vec = h_vec;
    cudaDeviceSynchronize();
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&H2Dtime, start, stop);
    std::cout <<"H2D =\t" << H2Dtime <<"\tms" <<std::endl;

    // Sort
    cudaEventRecord(start);
    thrust::sort(d_vec.begin(),d_vec.end());
    cudaDeviceSynchronize();
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&sortTime, start, stop);
    std::cout <<"sort =\t" << sortTime <<"\tms" <<std::endl;

    //D2H
    cudaEventRecord(start);
    h_vec_2 = d_vec;
    cudaDeviceSynchronize();
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&D2Htime, start, stop);
    std::cout <<"D2H =\t" << D2Htime <<"\tms" <<std::endl;

    cudaEventRecord(tock);
    cudaEventSynchronize(tock);
    cudaEventElapsedTime(&fullMeasure, tick, tock);
    std::cout <<"full measure =\t" << fullMeasure <<"\tms" <<std::endl;

}
