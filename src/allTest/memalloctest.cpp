#include <iostream>
#include <chrono>
#include <vector>


std::chrono::time_point<std::chrono::steady_clock> get_time (){

	std::chrono::time_point<std::chrono::steady_clock> tick = std::chrono::steady_clock::now();
	return tick;		
}

int main (){

    double ms;
    int dimension = 10000000;
    std::chrono::duration <double> duration;
    std::chrono::time_point<std::chrono::steady_clock> tic,toc;
    
    tic = get_time();
    std::vector<int> vec (dimension);
    toc = get_time();

    duration = toc - tic; 
    ms = duration.count() * 1000.0f;
    std::cout << "malloc took =\t" << ms << "\tms" << std::endl;
    


    return 0;
}