#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <iostream>
#include <chrono>

std::chrono::time_point<std::chrono::steady_clock> get_time (){

	std::chrono::time_point<std::chrono::steady_clock> tick = std::chrono::steady_clock::now();
	return tick;		
}

int main (int argc, char const* argv[]) {

    if (argc < 2){
		std::cout << "Please specify dimension as a command line argument " << std::endl;
		exit (-1);
  }
  int dimension = atoi(argv[1]) ; 

  // chrono parameters
  double ms;
  std::chrono::duration <double> duration;
  std::chrono::time_point<std::chrono::steady_clock> tic,toc;


  // Measure warm up
tic = get_time();
thrust::device_vector <int> dummy_vec (dimension);
//cudaDeviceSynchronize(); //no significant impact, warm-up goes from 190ms to 300ms anyway 
toc = get_time();

  // Display time
  duration = toc - tic; 
  ms = duration.count() * 1000.0f;
  std::cout << "Warm-up duration =\t" << ms << "\tms" << std::endl;







    return 0;
}