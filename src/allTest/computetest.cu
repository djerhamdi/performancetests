#include <thrust/execution_policy.h>
#include <thrust/transform_reduce.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/sequence.h>
#include <thrust/sort.h>
#include <algorithm>
#include <iostream>
#include <chrono>
#include <vector>

struct funct_gpu{
    const int k;

    funct_gpu(int _k) : k(_k) {}

    __host__ __device__
     	float operator () (const int& tab,const int& seq) const 
	{
	return (tab+k*seq)/(seq+1);
	}
};

std::chrono::time_point<std::chrono::steady_clock> get_time (){

	std::chrono::time_point<std::chrono::steady_clock> tick = std::chrono::steady_clock::now();
	return tick;		
}
float compute_cpu (int k, std::vector<int> tab){
	float R=0;
	for (uint i=0; i< tab.size(); i++){
		R=R+(tab[i]+k*i)/(i+1);
	}

	return R;
}
    
void  compute_gpu(int K, float& R , thrust::device_vector<int>& X, thrust::device_vector<int>& Y){
	// tab <- (tab * 10 * i) / seq
	thrust::transform(X.begin(), X.end(), Y.begin(), X.begin(), funct_gpu(K));
	// sum(tab)
	R=0;
	R = thrust::reduce(X.begin(), X.end(), (float) 0, thrust::plus<float>());
}

int main (int argc, char const* argv[]) {

    if (argc < 2){
          std::cout << "Please specify dimension as a command line argument " << std::endl;
          exit (-1);
    }
    int dimension = atoi(argv[1]) ; 

    // chrono parameters
    double ms;
    std::chrono::duration <double> duration;
    std::chrono::time_point<std::chrono::steady_clock> tic,toc;

    // Measure warm up
    cudaEvent_t start,stop, tick, tock;
    tic = get_time();
    cudaEventCreate(&start);
    toc = get_time();
    
    // cudaEvent parameters
    cudaEventCreate(&stop);
    cudaEventCreate(&tick);
    cudaEventCreate(&tock);
    float gpuComputeTime = 0 ,gpuFullTime = 0,seqGen = 0, memAlloc = 0, H2Dtime = 0;
  
    // Display warm-up time
    duration = toc - tic; 
    ms = duration.count() * 1000.0f;
    std::cout << "Warm-up =\t" << ms << "\tms" << std::endl;

    // Declaration 
    float S_cpu = 0,R_gpu = 0;
    int constant = 10;
    
    thrust::host_vector <int> h_vec (dimension);
    thrust::generate(h_vec.begin(), h_vec.end(), rand);


    
    cudaEventRecord(tick);

    // malloc
    cudaEventRecord(start);
    thrust::device_vector <int> d_tab (dimension);
    thrust::device_vector <int> i_seq(dimension);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&memAlloc, start, stop);
    std::cout <<"Device malloc  for " <<dimension<< " =\t" << memAlloc <<"\tms" <<std::endl;

    // H2D
    cudaEventRecord(start);
    d_tab = h_vec;
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&H2Dtime, start, stop);
    std::cout <<"H2D  for " <<dimension<< " =\t" << H2Dtime <<"\tms" <<std::endl;

    // seq generation
    cudaEventRecord(start);
    thrust::sequence(thrust::device,i_seq.begin(),i_seq.end(),0);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&seqGen, start, stop);
    std::cout <<"sequence generation of " <<dimension<< " =\t" << seqGen <<"\tms" <<std::endl;

    // compute_gpu
    cudaEventRecord(start);
    compute_gpu(constant, R_gpu,d_tab,i_seq);
    //cudaDeviceSynchronize();
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    std::cout<<R_gpu<<std::endl;
    cudaEventElapsedTime(&gpuComputeTime, start, stop);
    std::cout <<"calculation on gpu for " << dimension<<" took = " << gpuComputeTime <<"\tms"<<std::endl ;	

    cudaEventRecord(tock);
    cudaEventSynchronize(tock);
    cudaEventElapsedTime(&gpuFullTime, tick, tock);
	std::cout <<"calculation & memery allocation on gpu for " << dimension<<" took = " << gpuFullTime <<"\tms"<<std::endl ;	

    // CPU

    // cpu malloc
    tic = get_time();
    std::vector<int> Table (dimension);
    toc = get_time();

    // Display cpu malloc time
    duration = toc - tic; 
    ms = duration.count() * 1000.0f;
    std::cout << "CPU malloc for " <<dimension<< "  =\t" << ms << "\tms" << std::endl;
 
    // H2std
    thrust::copy(h_vec.begin(),h_vec.end(),Table.begin());

    // compute cpu
    tic = get_time();
    S_cpu = compute_cpu (constant,Table);
    std::cout<<S_cpu<<std::endl;
    toc = get_time();
    
    // Display compute  cpu time
	duration = toc - tic;
	ms =duration.count() * 1000.0f;
	std::cout <<"calculation on cpu for " << dimension<<" took = " << ms <<"\tms"<<std::endl ;	




    return 0;
}
