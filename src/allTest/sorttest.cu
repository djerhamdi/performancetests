#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/sort.h>
#include <algorithm>
#include <iostream>
#include <chrono>

std::chrono::time_point<std::chrono::steady_clock> get_time (){

	std::chrono::time_point<std::chrono::steady_clock> tick = std::chrono::steady_clock::now();
	return tick;		
}


int main (int argc, char const* argv[]) {

    if (argc < 2){
          std::cout << "Please specify dimension as a command line argument " << std::endl;
          exit (-1);
    }
    int dimension = atoi(argv[1]) ; 

    // chrono parameters
    double ms;
    std::chrono::duration <double> duration;
    std::chrono::time_point<std::chrono::steady_clock> tic,toc;
    
    // Measure warm up
    cudaEvent_t start,stop, tick, tock;
    tic = get_time();
    cudaEventCreate(&start);
    toc = get_time();
    
    // cudaEvent parameters
    cudaEventCreate(&stop);
    cudaEventCreate(&tick);
    cudaEventCreate(&tock);
    float H2Dtime = 0 ,D2Htime = 0, fullMeasure = 0, sortTime = 0, memAlloc = 0; 
    

    // Display warm-up time
    duration = toc - tic; 
    ms = duration.count() * 1000.0f;
    std::cout << "Warm-up =\t" << ms << "\tms" << std::endl;
    
    thrust::host_vector <int> h_vec (dimension);
    thrust::host_vector <int> h_vec_2 (dimension);
    thrust::generate(h_vec.begin(), h_vec.end(), rand);

    
    cudaEventRecord(tick);

    // malloc
    cudaEventRecord(start);
    thrust::device_vector <int> d_vec (dimension);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&memAlloc, start, stop);
    std::cout <<"Device malloc  for " <<dimension<< " =\t" << memAlloc <<"\tms" <<std::endl;

   
    //H2D
    cudaEventRecord(start);
    d_vec = h_vec;
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&H2Dtime, start, stop);
    std::cout <<"H2D  for " <<dimension<< " =\t" << H2Dtime <<"\tms" <<std::endl;

    // thrust::sort
    cudaEventRecord(start);
    thrust::sort(d_vec.begin(),d_vec.end());
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&sortTime, start, stop);
    std::cout <<"GPU sort for " <<dimension<< " =\t" << sortTime <<"\tms" <<std::endl;

    //D2H
    cudaEventRecord(start);
    h_vec_2 = d_vec;
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&D2Htime, start, stop);
    std::cout <<"D2H  for " <<dimension<< " =\t" << D2Htime <<"\tms" <<std::endl;
    
    cudaEventRecord(tock);
    cudaEventSynchronize(tock);
    cudaEventElapsedTime(&fullMeasure, tick, tock);
    std::cout <<"GPU full measure for " <<dimension<< " =\t" << fullMeasure <<"\tms" <<std::endl;

// CPU
    // cpu malloc
    tic = get_time();
    std::vector <int> std_vec (dimension);
    toc = get_time();

    // Display cpu malloc time
    duration = toc - tic; 
    ms = duration.count() * 1000.0f;
    std::cout << "CPU malloc for " <<dimension<< "  =\t" << ms << "\tms" << std::endl;
    
    // H2std
    thrust::copy(h_vec.begin(),h_vec.end(),std_vec.begin());

    // std::sort
    tic = get_time();
    std::sort(std_vec.begin(),std_vec.end());
    toc = get_time();

    // Display cpu sort time
    duration = toc - tic; 
    ms = duration.count() * 1000.0f;
    std::cout << "CPU sort for " <<dimension<< "  =\t" << ms << "\tms" << std::endl;

}
