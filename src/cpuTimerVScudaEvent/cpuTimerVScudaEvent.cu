#include <iostream>
#include <sys/time.h>
#include "perflibcuda.h"



int main (int argc, char const* argv[]) {
	if (argc < 2){
		std::cout << "Please specify dimension as a command line arguemnt \n";
		exit (-1);
	}
	int dimension = atoi(argv[1]) ;

	float R1,R2;
	int cte1 = 10;
	int cte2 = 20;
	
	std::chrono::duration <double> duration;
	std::chrono::time_point<std::chrono::steady_clock> tic,toc;
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	double ms;
	float milliseconds = 0;
	
	redVScal obj;
	thrust::host_vector<float> Table (dimension);
	thrust::device_vector <float> d_tab(dimension);
	thrust::device_vector <float> i_seq(dimension);
	thrust::sequence(thrust::device,i_seq.begin(),i_seq.end(),1);

	cudaEventRecord(start);
	obj.compute_gpu(cte1, R1,d_tab,i_seq);
	cudaEventRecord(stop);
	cudaEventSynchronize(stop); 

	tic=obj.get_time();
	obj.compute_gpu(cte2, R2,d_tab,i_seq);
	cudaDeviceSynchronize();
	toc=obj.get_time();
	
	duration = toc - tic;
	ms =duration.count() * 1000.0f;
	
	cudaEventElapsedTime(&milliseconds, start, stop);
	
	
	std::cout<<R1<<std::endl;
	std::cout<<R2<<std::endl;
	
	std::cout <<"reduce function using CPU timers took =" << ms << "\tms"<<std::endl ;
	std::cout <<"reduce function using cudaEvent took =" << milliseconds << "ms"<<"\tms"<<std::endl ;	

	
	return  0;
	
}
