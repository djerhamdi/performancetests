#include "perflibcuda.h"


float redVScal:: compute_cpu (int k, thrust::host_vector<float> tab){
	double R=0;
	for (uint i=0; i<= tab.size(); i++){
		R=R+(tab[i]+k*i)/(i+1);
		}

	return R;
	}

void  redVScal::compute_gpu(int K, float& R , thrust::device_vector<float>& X, thrust::device_vector<float>& Y)
	{
		// tab <- (tab * 10 * i) / seq
		thrust::transform(X.begin(), X.end(), Y.begin(), X.begin(), funct_gpu(K));
		// sum(tab)
		R=0;
		R = thrust::reduce(X.begin(), X.end(), (float) 0, thrust::plus<float>());
}

std::chrono::time_point<std::chrono::steady_clock> redVScal::get_time ()
{
	std::chrono::time_point<std::chrono::steady_clock> tick = std::chrono::steady_clock::now();
	return tick;		
}


