#include <gtest/gtest.h>
#include "perflibcuda.h"

class test_redVScal: public ::testing::Test{
protected:
	int xdim = 3;
	std::vector<int> Tt1;	

	redVScal obj;
	int constant = 10;
	
void SetUp() override {
	clearTable();
}

	void clearTable() {
	
	Tt1.clear();
		for (int i = 1; i <= xdim; i++){ 
       		Tt1.push_back(0); 
  		}
	}
};


TEST_F(test_redVScal,calculation){
	//given	
	float S;
	//when
	S= obj.compute_cpu(constant, Tt1);
	
	//then
	ASSERT_EQ(115,S*6)<< "Compute function failed";	
}



TEST_F(test_redVScal,computation_gpu){
	//given	
	thrust::device_vector <float> i_seq (xdim);
	thrust::sequence(thrust::device,i_seq.begin(),i_seq.end(),1);
	thrust::device_vector<float> d_Tt1= Tt1;
	float R;
	
	//when
	
	obj.compute_gpu(constant, R, d_Tt1, i_seq);
	//then
	ASSERT_EQ(115,R*6)<< "compute function failed";	
}

