#include <iostream>
#include <sys/time.h>
#include "perflibcuda.h"


int main (int argc, char const* argv[]) {

	// read size from argument
	if (argc < 2){
		std::cout << "Please specify dimension as a command line argument \n";
		exit (-1);
	}
	int dimension = atoi(argv[1]) ;
	
	// declaration
	std::chrono::duration <double> duration;
	double ms1; 
	std::chrono::time_point<std::chrono::steady_clock> tic,toc;
	float S_cpu,R_gpu;
	int constant = 10;
	redVScal obj;

	cudaEvent_t start1, stop1, start2, stop2 ;
	cudaEventCreate(&start1);
	cudaEventCreate(&stop1);
	cudaEventCreate(&start2);
	cudaEventCreate(&stop2);
	float gpuComputeTime = 0;
	float gpuFullTime = 0;

	thrust::host_vector<float> Table (dimension);
	// cpu
	tic = obj.get_time();
	S_cpu = obj.compute_cpu (constant,Table);
	toc = obj.get_time();
	duration = toc - tic;
	ms1 =duration.count() * 1000.0f;
	std::cout <<"calculation on cpu for " << dimension<<" took = " << ms1 <<"\tms"<<std::endl ;	

	
	
	cudaEventRecord(start1);
	thrust::device_vector <float> d_tab(dimension);
	thrust::device_vector <float> i_seq(dimension);
	thrust::sequence(thrust::device,i_seq.begin(),i_seq.end(),1);
	// gpu
	cudaEventRecord(start2);
	obj.compute_gpu(constant, R_gpu,d_tab,i_seq);
	cudaDeviceSynchronize();
	cudaEventRecord(stop2);
	cudaEventSynchronize(stop2); 
	cudaEventRecord(stop1);
	cudaEventSynchronize(stop1); 
	cudaEventElapsedTime(&gpuComputeTime, start2, stop2);
	cudaEventElapsedTime(&gpuFullTime, start1, stop1);
	std::cout <<"calculation on gpu for " << dimension<<" took = " << gpuComputeTime <<"\tms"<<std::endl ;	
	std::cout <<"calculation & memery allocation on gpu for " << dimension<<" took = " << gpuFullTime <<"\tms"<<std::endl ;	

	return  0;
}

