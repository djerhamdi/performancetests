#pragma once
#include <sys/time.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/sequence.h>
#include <thrust/execution_policy.h>
#include <thrust/transform_reduce.h>
#include <thrust/functional.h>
#include <chrono>

class redVScal {
public:
struct funct_gpu
{
    const int k;

    funct_gpu(int _k) : k(_k) {}

    __host__ __device__
     	float operator () (const float& tab,const float& seq) const 
	{
	return (tab+k*seq)/(seq+1);
	}
};

	float compute_cpu (int k ,thrust::host_vector<float> tab);
	void compute_gpu(int K, float& R , thrust::device_vector<float>& X, thrust::device_vector<float>& Y);
	std::chrono::time_point<std::chrono::steady_clock> get_time ();
};
