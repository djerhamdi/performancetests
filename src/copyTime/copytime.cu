#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <iostream>
#include <chrono>

void forceDriverInitWithDeviceMemAllocation() {
  thrust::device_vector<int> dummy_vec (1);
}

std::chrono::time_point<std::chrono::steady_clock> get_time (){

	std::chrono::time_point<std::chrono::steady_clock> tick = std::chrono::steady_clock::now();
	return tick;		
}

void measureWarmUpTime() {

  double ms;
  std::chrono::duration <double> duration;
  std::chrono::time_point<std::chrono::steady_clock> tic,toc;
  
  // Measurement
  tic=get_time();
  forceDriverInitWithDeviceMemAllocation();
  cudaDeviceSynchronize();
  toc=get_time();
  
  // Display time
  duration = toc - tic;
  ms =duration.count() * 1000.0f;
  std::cout<<"Warm-up duration = " << ms << "\tms" << std::endl;
}


void deviceMemoryAllocation(int dimension) {
  
  // Create a Cuda event
  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);
  float elapsed = 0;
  
  // Measurement
  cudaEventRecord(start);	
  thrust::device_vector <int> d_vec (dimension);
  cudaEventRecord(stop);
  cudaEventSynchronize(stop);
  
  // Display time
  cudaEventElapsedTime(&elapsed, start, stop);
  std::cout<<"Device memory allocation time = " << elapsed << "\tms of size " << d_vec.size() << std::endl;
}

int main (int argc, char const* argv[]) {

  if (argc < 2){
		std::cout << "Please specify dimension as a command line argument " << std::endl;
		exit (-1);
  }
  int dimension = atoi(argv[1]) ; 
  

  // driver initiation and memory allocation
  measureWarmUpTime();  
  deviceMemoryAllocation(dimension);
  
  // Create a Cuda event
  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);
  float elapsed = 0; // time in ms

  // initialized vectors
  thrust::host_vector <int> h_vec (dimension, 1);
  thrust::device_vector <int> d_vec (dimension);
  thrust::host_vector <int> h_vec_2 (dimension);

  // H2D:
  cudaEventRecord(start);
  d_vec = h_vec;
  cudaEventRecord(stop);
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&elapsed, start, stop);
  std::cout<<"H2D elapsed time for "<<dimension<<" = " << elapsed << "\tms"<<std::endl;

  // D2H:
  cudaEventRecord(start);
  h_vec_2 = d_vec;
  cudaEventRecord(stop);
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&elapsed, start, stop);
  std::cout<<"D2H elapsed time for "<<dimension<<" = " << elapsed << "\tms"<<std::endl;

}