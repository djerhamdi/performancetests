#include <gtest/gtest.h>
#include "perflib.h"
#include <vector>


class test_perfTest_returnVSreference: public ::testing::Test {

	protected:
	int xdim = 100;
	std::vector<int> Tt1;	

void SetUp() override {
	clearTable();
}

	void clearTable() {
	
	Tt1.clear();
		for (int i = 1; i <= xdim; i++){ 
       		Tt1.push_back(0); 
  		}
	}


};


TEST_F(test_perfTest_returnVSreference,addOne_test){
	//given	
	perfTest_returnVSreference obj;
	int sum = 0;
	//when
	Tt1=obj.addOneTable(Tt1,xdim);

	
	//then
	sum = std::accumulate(Tt1.begin(), Tt1.end(), 0);
	ASSERT_EQ(sum,100) << "addOneTableFunction failed";
}
