#pragma once
#include <iostream>
#include <vector>
#include <numeric>
#include <sys/time.h>
#include <chrono>



class perfTest_returnVSreference{
public:

double get_timeofday();

std::chrono::time_point<std::chrono::steady_clock> get_time ();
std::vector <int> addOneTable (std::vector <int> T1,int size);
std::vector <int> addOneTablebyRef (std::vector <int> &T1,int size);

};
