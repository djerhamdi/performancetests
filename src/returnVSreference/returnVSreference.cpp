#include "perflib.h"

int main (int argc, char const* argv[]) {
	if (argc < 2){
		std::cout << "Please specify dimension as a command line arguemnt \n";
		exit (-1);
	}
	int dimension = atoi(argv[1]) ;
	std::chrono::time_point<std::chrono::steady_clock> tic,toc;
	std::chrono::duration <double> duration;
	double tic1,toc1,ms; 
	int sum;
	perfTest_returnVSreference obj;
	tic = obj.get_time();
	tic1 = obj.get_timeofday();
	std::vector <int> Table (dimension);
	Table = obj.addOneTable (Table,dimension);
	sum = std::accumulate(Table.begin(), Table.end(), 0);
	std::cout<< sum <<std::endl;
	toc=obj.get_time();
	toc1 = obj.get_timeofday();
	duration = toc - tic;
	ms =duration.count() * 1000.0f;
	std::cout <<"Elapsed time w/o reference=" << ms << "ms"<<std::endl ;
	std::cout <<"Elapsed timeofday w/o reference=" << (toc1-tic1)*1e3 << "ms"<<std::endl ;
	tic1 = obj.get_timeofday();
	tic = obj.get_time();
	std::vector <int> TableR (dimension);
	TableR = obj.addOneTablebyRef (TableR,dimension);
	sum = std::accumulate(TableR.begin(), TableR.end(), 0);
	std::cout<< sum <<std::endl;
	toc1 = obj.get_timeofday();
	toc=obj.get_time();
	duration = toc - tic;
	ms =duration.count() * 1000.0f;
	std::cout <<"Elapsed time w/ reference=" << ms<< "ms"<<std::endl ;
	std::cout <<"Elapsed timeofday w reference=" << (toc1-tic1)*1e3 << "ms"<<std::endl ;
	return 0;
}
