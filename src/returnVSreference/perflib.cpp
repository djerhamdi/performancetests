
#include "perflib.h"

 


double perfTest_returnVSreference::get_timeofday(){
	struct timeval t;
	struct timezone tzp;
	gettimeofday(&t, &tzp);
	return t.tv_sec + t.tv_usec*1e-6;

}




std::chrono::time_point<std::chrono::steady_clock> perfTest_returnVSreference::get_time ()
{
	std::chrono::time_point<std::chrono::steady_clock> tick = std::chrono::steady_clock::now();
	return tick;		
}

std::vector <int> perfTest_returnVSreference::addOneTable (std::vector <int> T1,int size){
	for (int i=0; i<size;i++){
		T1[i] =T1[i]+1;

		}
	return T1;
}

std::vector <int> perfTest_returnVSreference::addOneTablebyRef (std::vector <int> &T1,int size){
	for (int i=0; i<=size;i++){
		T1[i] =T1[i]+1;
		}

	return T1;
}

