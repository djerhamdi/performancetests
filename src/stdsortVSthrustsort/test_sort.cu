#include <gtest/gtest.h>
#include "sort_comp.h"


class sortclass_test: public ::testing::Test {

protected:
	thrust::device_vector<int> d_t;
	std::vector <int> h_t;	
	uint32_t xDimt = 10;
	
	void SetUp() override {
		resetTabs();
	}

	void resetTabs() {
		d_t.clear();
		h_t.clear();
	}
};

TEST_F(sortclass_test, stdsortTest) {
	// given
	sortclass obj;
	
	for (uint32_t i = xDimt; i >= 1; i--) {
		h_t.push_back(i);
	}
	
	// when
	obj.stdSort(h_t);
	
	// then
	for (uint32_t i = 0; i < h_t.size() - 1; i++) {
		ASSERT_LE(h_t[i], h_t[i+1])<< "sorting tables failed";
	}
}

TEST_F(sortclass_test, thrustsortTest) {
	// given
	sortclass obj;
	
	for (uint32_t i = xDimt; i >= 1; i--) {
		d_t.push_back(i);
	}
	
	// when
	obj.thrustSort(d_t);
	
	// then
	for (uint32_t i = 0; i < d_t.size() - 1; i++) {
		ASSERT_LE(d_t[i], d_t[i+1])<< "sorting tables failed";
	}
}

