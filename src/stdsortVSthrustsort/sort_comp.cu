#include "sort_comp.h"



  

int main (int argc, char const* argv[]) {
	if (argc < 2){
		std::cout << "Please specify dimension as a command line argument \n";
		exit (-1);
	}
	int dimension = atoi(argv[1]) ;
	
	sortclass obj;
	float stdTime, thrustTime, gpuFullTime;
	std::chrono::duration <double> duration;
	std::chrono::time_point<std::chrono::steady_clock> tic,toc;
	cudaEvent_t start1, stop1, start2, stop2;
	cudaEventCreate(&start1);
 	cudaEventCreate(&stop1);
	cudaEventCreate(&start2);
 	cudaEventCreate(&stop2);
	
	// Table initiation
	std::vector <int> h_T (dimension);
	thrust::device_vector <int> d_T (dimension);
	thrust::host_vector<int> randTable(dimension);
	thrust::host_vector<int> sortedTable(dimension);

	// fill Table Randomly
	obj.fillTableRandomly(randTable);
	
	// copy random table to std::vector
	thrust::copy(randTable.begin(), randTable.end(), h_T.begin());
	

	// std::sort
	tic = obj.get_time();
	obj.stdSort(h_T);
	toc = obj.get_time();
	duration = toc - tic;
	stdTime = duration.count() * 1000.0f;
	std::cout << "std::sort for " <<dimension<< " took = " << stdTime <<"\tms"<< std::endl;

	cudaEventRecord(start2);
	// H2D
	d_T = randTable;
	cudaDeviceSynchronize();
	// thrust::sort
	cudaEventRecord(start1);
	obj.thrustSort(d_T);
	cudaDeviceSynchronize();
	cudaEventRecord(stop1);
	cudaEventSynchronize(stop1);
	cudaEventElapsedTime(&thrustTime, start1, stop1);

	// D2H
	sortedTable = d_T;
	cudaDeviceSynchronize();
	cudaEventRecord(stop2);
	cudaEventSynchronize(stop2);
	cudaEventElapsedTime(&gpuFullTime, start2, stop2);

	std::cout << "thrust::sort for " <<dimension<< " took = " << thrustTime <<"\tms"<< std::endl;
	std::cout << "copy + sort on gpu for " <<dimension<< " took = " << gpuFullTime <<"\tms"<< std::endl;
	return 0;
}
