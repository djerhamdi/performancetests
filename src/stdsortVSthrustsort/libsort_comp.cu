#include "sort_comp.h"

 
void sortclass::fillTableRandomly(thrust::host_vector <int> &tab1){
	tab1.clear();
	thrust::generate(tab1.begin(),tab1.end(),rand);
}

void sortclass::stdSort(std::vector <int> &vec1){
	std::sort(vec1.begin(), vec1.end());
}

void sortclass::thrustSort(thrust::device_vector <int> &vec1){
	thrust::sort(vec1.begin(), vec1.end());
}

std::chrono::time_point<std::chrono::steady_clock> sortclass::get_time ()
{
	std::chrono::time_point<std::chrono::steady_clock> tick = std::chrono::steady_clock::now();
	return tick;		
}
