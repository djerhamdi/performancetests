/*
 * vectoralgo0.h
 *
 *  Created on: Mar 18, 2020
 *      Author: med
 */
#pragma once
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <iostream>
#include <sys/time.h>
#include <algorithm>
#include <chrono>

//#define NDEBUG 	// uncomment to disable assertions

class sortclass{

public:
	void fillTableRandomly(thrust::host_vector <int> &tab1);
	void  stdSort(std::vector <int> &vec1);
	void  thrustSort(thrust::device_vector <int> &vec1);
	std::chrono::time_point<std::chrono::steady_clock> get_time ();
};



