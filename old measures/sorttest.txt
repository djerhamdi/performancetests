Warm-up =	161.681	ms
Device malloc  == 1 =	0.947584	ms
H2D  == 1 =	0.019072	ms
GPU sort == 1 =	0.110624	ms
D2H  == 1 =	0.020704	ms
GPU full measure == 1 =	1.16499	ms
CPU malloc == 1  =	0.000503	ms
CPU sort == 1  =	0.000881	ms
Warm-up =	143.735	ms
Device malloc  == 10 =	0.2024	ms
H2D  == 10 =	0.01952	ms
GPU sort == 10 =	0.104672	ms
D2H  == 10 =	0.024384	ms
GPU full measure == 10 =	0.43088	ms
CPU malloc == 10  =	0.000259	ms
CPU sort == 10  =	0.002507	ms
Warm-up =	130.867	ms
Device malloc  == 50 =	0.728096	ms
H2D  == 50 =	0.016032	ms
GPU sort == 50 =	0.091392	ms
D2H  == 50 =	0.02048	ms
GPU full measure == 50 =	0.920096	ms
CPU malloc == 50  =	0.000601	ms
CPU sort == 50  =	0.007256	ms
Warm-up =	125.271	ms
Device malloc  == 100 =	0.702752	ms
H2D  == 100 =	0.017344	ms
GPU sort == 100 =	0.094016	ms
D2H  == 100 =	0.020704	ms
GPU full measure == 100 =	0.9	ms
CPU malloc == 100  =	0.000716	ms
CPU sort == 100  =	0.014813	ms
Warm-up =	126.54	ms
Device malloc  == 200 =	0.700704	ms
H2D  == 200 =	0.015872	ms
GPU sort == 200 =	0.09456	ms
D2H  == 200 =	0.020736	ms
GPU full measure == 200 =	0.897984	ms
CPU malloc == 200  =	0.000834	ms
CPU sort == 200  =	0.031209	ms
Warm-up =	122.695	ms
Device malloc  == 300 =	0.320768	ms
H2D  == 300 =	0.017632	ms
GPU sort == 300 =	0.088832	ms
D2H  == 300 =	0.020992	ms
GPU full measure == 300 =	0.515584	ms
CPU malloc == 300  =	0.00129	ms
CPU sort == 300  =	0.049344	ms
Warm-up =	122.939	ms
Device malloc  == 400 =	0.318944	ms
H2D  == 400 =	0.0168	ms
GPU sort == 400 =	0.08896	ms
D2H  == 400 =	0.021024	ms
GPU full measure == 400 =	0.511424	ms
CPU malloc == 400  =	0.001534	ms
CPU sort == 400  =	0.06752	ms
Warm-up =	123.312	ms
Device malloc  == 500 =	0.373824	ms
H2D  == 500 =	0.01712	ms
GPU sort == 500 =	0.091968	ms
D2H  == 500 =	0.022272	ms
GPU full measure == 500 =	0.570496	ms
CPU malloc == 500  =	0.001836	ms
CPU sort == 500  =	0.087225	ms
Warm-up =	122.428	ms
Device malloc  == 600 =	0.33952	ms
H2D  == 600 =	0.017344	ms
GPU sort == 600 =	0.092416	ms
D2H  == 600 =	0.021472	ms
GPU full measure == 600 =	0.536864	ms
CPU malloc == 600  =	0.001851	ms
CPU sort == 600  =	0.105599	ms
Warm-up =	122.967	ms
Device malloc  == 700 =	0.323712	ms
H2D  == 700 =	0.017152	ms
GPU sort == 700 =	0.093408	ms
D2H  == 700 =	0.021728	ms
GPU full measure == 700 =	0.521536	ms
CPU malloc == 700  =	0.002084	ms
CPU sort == 700  =	0.124814	ms
Warm-up =	123.366	ms
Device malloc  == 800 =	0.379456	ms
H2D  == 800 =	0.017216	ms
GPU sort == 800 =	0.107104	ms
D2H  == 800 =	0.022528	ms
GPU full measure == 800 =	0.595296	ms
CPU malloc == 800  =	0.002414	ms
CPU sort == 800  =	0.169117	ms
Warm-up =	123.188	ms
Device malloc  == 900 =	0.327328	ms
H2D  == 900 =	0.018304	ms
GPU sort == 900 =	0.09088	ms
D2H  == 900 =	0.020864	ms
GPU full measure == 900 =	0.523488	ms
CPU malloc == 900  =	0.002544	ms
CPU sort == 900  =	0.168468	ms
Warm-up =	122.777	ms
Device malloc  == 1000 =	0.315456	ms
H2D  == 1000 =	0.018208	ms
GPU sort == 1000 =	0.093056	ms
D2H  == 1000 =	0.021856	ms
GPU full measure == 1000 =	0.513728	ms
CPU malloc == 1000  =	0.002633	ms
CPU sort == 1000  =	0.18737	ms
Warm-up =	124.507	ms
Device malloc  == 3000 =	0.320768	ms
H2D  == 3000 =	0.021568	ms
GPU sort == 3000 =	0.390208	ms
D2H  == 3000 =	0.024128	ms
GPU full measure == 3000 =	0.821472	ms
CPU malloc == 3000  =	0.006499	ms
CPU sort == 3000  =	0.632417	ms
Warm-up =	124.223	ms
Device malloc  == 5000 =	0.327328	ms
H2D  == 5000 =	0.023936	ms
GPU sort == 5000 =	0.408608	ms
D2H  == 5000 =	0.027136	ms
GPU full measure == 5000 =	0.854304	ms
CPU malloc == 5000  =	0.010415	ms
CPU sort == 5000  =	1.11207	ms
Warm-up =	118.993	ms
Device malloc  == 7000 =	0.321376	ms
H2D  == 7000 =	0.026304	ms
GPU sort == 7000 =	0.404576	ms
D2H  == 7000 =	0.029952	ms
GPU full measure == 7000 =	0.850016	ms
CPU malloc == 7000  =	0.027072	ms
CPU sort == 7000  =	1.61391	ms
Warm-up =	123.021	ms
Device malloc  == 10000 =	0.370528	ms
H2D  == 10000 =	0.03056	ms
GPU sort == 10000 =	0.384256	ms
D2H  == 10000 =	0.033312	ms
GPU full measure == 10000 =	0.88288	ms
CPU malloc == 10000  =	0.040547	ms
CPU sort == 10000  =	2.38363	ms
Warm-up =	122.916	ms
Device malloc  == 50000 =	0.299296	ms
H2D  == 50000 =	0.07632	ms
GPU sort == 50000 =	0.423008	ms
D2H  == 50000 =	0.08688	ms
GPU full measure == 50000 =	0.959008	ms
CPU malloc == 50000  =	0.190406	ms
CPU sort == 50000  =	13.8359	ms
Warm-up =	123.374	ms
Device malloc  == 100000 =	0.303008	ms
H2D  == 100000 =	0.12752	ms
GPU sort == 100000 =	0.446976	ms
D2H  == 100000 =	0.152736	ms
GPU full measure == 100000 =	1.10272	ms
CPU malloc == 100000  =	0.381383	ms
CPU sort == 100000  =	29.3674	ms
Warm-up =	123.056	ms
Device malloc  == 300000 =	0.181696	ms
H2D  == 300000 =	0.398656	ms
GPU sort == 300000 =	1.08496	ms
D2H  == 300000 =	0.40224	ms
GPU full measure == 300000 =	2.14915	ms
CPU malloc == 300000  =	1.11572	ms
CPU sort == 300000  =	95.2715	ms
Warm-up =	122.947	ms
Device malloc  == 500000 =	0.191008	ms
H2D  == 500000 =	0.542272	ms
GPU sort == 500000 =	1.24339	ms
D2H  == 500000 =	0.54624	ms
GPU full measure == 500000 =	2.60723	ms
CPU malloc == 500000  =	1.9433	ms
CPU sort == 500000  =	165.951	ms
Warm-up =	123.329	ms
Device malloc  == 700000 =	0.288032	ms
H2D  == 700000 =	0.730784	ms
GPU sort == 700000 =	1.51082	ms
D2H  == 700000 =	0.6992	ms
GPU full measure == 700000 =	3.31472	ms
CPU malloc == 700000  =	2.6032	ms
CPU sort == 700000  =	237.353	ms
Warm-up =	123.084	ms
Device malloc  == 1000000 =	0.207488	ms
H2D  == 1000000 =	0.975776	ms
GPU sort == 1000000 =	1.79254	ms
D2H  == 1000000 =	0.948736	ms
GPU full measure == 1000000 =	4.0151	ms
CPU malloc == 1000000  =	3.86864	ms
CPU sort == 1000000  =	349.086	ms
Warm-up =	122.685	ms
Device malloc  == 5000000 =	0.393024	ms
H2D  == 5000000 =	5.01206	ms
GPU sort == 5000000 =	6.42003	ms
D2H  == 5000000 =	3.2527	ms
GPU full measure == 5000000 =	15.1938	ms
CPU malloc == 5000000  =	18.346	ms
CPU sort == 5000000  =	1941.31	ms
Warm-up =	135.561	ms
Device malloc  == 10000000 =	0.832256	ms
H2D  == 10000000 =	11.7877	ms
GPU sort == 10000000 =	11.7279	ms
D2H  == 10000000 =	6.20061	ms
GPU full measure == 10000000 =	30.6835	ms
CPU malloc == 10000000  =	36.4489	ms
CPU sort == 10000000  =	4038.95	ms
Warm-up =	140.642	ms
Device malloc  == 30000000 =	2.15459	ms
H2D  == 30000000 =	36.5926	ms
GPU sort == 30000000 =	32.7582	ms
D2H  == 30000000 =	44.3137	ms
GPU full measure == 30000000 =	115.968	ms
CPU malloc == 30000000  =	108.884	ms
CPU sort == 30000000  =	12971.5	ms
Warm-up =	119.775	ms
Device malloc  == 50000000 =	2.46445	ms
H2D  == 50000000 =	73.6778	ms
GPU sort == 50000000 =	53.7963	ms
D2H  == 50000000 =	73.3307	ms
GPU full measure == 50000000 =	203.424	ms
CPU malloc == 50000000  =	183.589	ms
CPU sort == 50000000  =	22145.9	ms
Warm-up =	141.238	ms
Device malloc  == 70000000 =	3.03686	ms
H2D  == 70000000 =	111.195	ms
GPU sort == 70000000 =	74.7127	ms
D2H  == 70000000 =	102.081	ms
GPU full measure == 70000000 =	291.182	ms
CPU malloc == 70000000  =	261.228	ms
CPU sort == 70000000  =	31649.1	ms
Warm-up =	141.982	ms
Device malloc  == 100000000 =	3.71728	ms
H2D  == 100000000 =	143.811	ms
GPU sort == 100000000 =	106.255	ms
D2H  == 100000000 =	145.707	ms
GPU full measure == 100000000 =	399.654	ms
CPU malloc == 100000000  =	382.386	ms
CPU sort == 100000000  =	46096.7	ms
Warm-up =	143.058	ms
Device malloc  == 1 =	1.07261	ms
H2D  == 1 =	0.01888	ms
GPU sort == 1 =	0.104224	ms
D2H  == 1 =	0.020832	ms
GPU full measure == 1 =	1.28573	ms
CPU malloc == 1  =	0.000571	ms
CPU sort == 1  =	0.00045	ms
Warm-up =	124.17	ms
Device malloc  == 10 =	0.825216	ms
H2D  == 10 =	0.017984	ms
GPU sort == 10 =	0.085088	ms
D2H  == 10 =	0.019104	ms
GPU full measure == 10 =	1.01171	ms
CPU malloc == 10  =	0.000213	ms
CPU sort == 10  =	0.002119	ms
Warm-up =	123.892	ms
Device malloc  == 50 =	0.313376	ms
H2D  == 50 =	0.016832	ms
GPU sort == 50 =	0.091776	ms
D2H  == 50 =	0.01936	ms
GPU full measure == 50 =	0.50736	ms
CPU malloc == 50  =	0.000528	ms
CPU sort == 50  =	0.007315	ms
Warm-up =	122.537	ms
Device malloc  == 100 =	0.317408	ms
H2D  == 100 =	0.015808	ms
GPU sort == 100 =	0.093184	ms
D2H  == 100 =	0.020704	ms
GPU full measure == 100 =	0.52336	ms
CPU malloc == 100  =	0.000773	ms
CPU sort == 100  =	0.014813	ms
Warm-up =	123.087	ms
Device malloc  == 200 =	0.375424	ms
H2D  == 200 =	0.016768	ms
GPU sort == 200 =	0.089152	ms
D2H  == 200 =	0.020288	ms
GPU full measure == 200 =	0.567872	ms
CPU malloc == 200  =	0.000861	ms
CPU sort == 200  =	0.031055	ms
Warm-up =	122.246	ms
Device malloc  == 300 =	0.375072	ms
H2D  == 300 =	0.016768	ms
GPU sort == 300 =	0.092768	ms
D2H  == 300 =	0.021312	ms
GPU full measure == 300 =	0.572448	ms
CPU malloc == 300  =	0.001274	ms
CPU sort == 300  =	0.04924	ms
Warm-up =	122.337	ms
Device malloc  == 400 =	0.377568	ms
H2D  == 400 =	0.01728	ms
GPU sort == 400 =	0.093696	ms
D2H  == 400 =	0.02192	ms
GPU full measure == 400 =	0.575296	ms
CPU malloc == 400  =	0.001428	ms
CPU sort == 400  =	0.067212	ms
Warm-up =	122.399	ms
Device malloc  == 500 =	0.336832	ms
H2D  == 500 =	0.015872	ms
GPU sort == 500 =	0.096896	ms
D2H  == 500 =	0.02176	ms
GPU full measure == 500 =	0.547552	ms
CPU malloc == 500  =	0.001846	ms
CPU sort == 500  =	0.091013	ms
Warm-up =	122.541	ms
Device malloc  == 600 =	0.352128	ms
H2D  == 600 =	0.017472	ms
GPU sort == 600 =	0.095328	ms
D2H  == 600 =	0.021856	ms
GPU full measure == 600 =	0.552672	ms
CPU malloc == 600  =	0.001908	ms
CPU sort == 600  =	0.105188	ms
Warm-up =	122.992	ms
Device malloc  == 700 =	0.334464	ms
H2D  == 700 =	0.017568	ms
GPU sort == 700 =	0.119936	ms
D2H  == 700 =	0.0224	ms
GPU full measure == 700 =	0.563072	ms
CPU malloc == 700  =	0.002153	ms
CPU sort == 700  =	0.136765	ms
Warm-up =	122.778	ms
Device malloc  == 800 =	0.333408	ms
H2D  == 800 =	0.017344	ms
GPU sort == 800 =	0.095136	ms
D2H  == 800 =	0.021376	ms
GPU full measure == 800 =	0.533952	ms
CPU malloc == 800  =	0.002244	ms
CPU sort == 800  =	0.14516	ms
Warm-up =	123.361	ms
Device malloc  == 900 =	0.348544	ms
H2D  == 900 =	0.017184	ms
GPU sort == 900 =	0.093184	ms
D2H  == 900 =	0.02144	ms
GPU full measure == 900 =	0.545056	ms
CPU malloc == 900  =	0.002449	ms
CPU sort == 900  =	0.16803	ms
Warm-up =	123.815	ms
Device malloc  == 1000 =	0.32048	ms
H2D  == 1000 =	0.018176	ms
GPU sort == 1000 =	0.09616	ms
D2H  == 1000 =	0.021984	ms
GPU full measure == 1000 =	0.523392	ms
CPU malloc == 1000  =	0.002628	ms
CPU sort == 1000  =	0.187251	ms
Warm-up =	122.268	ms
Device malloc  == 3000 =	0.389408	ms
H2D  == 3000 =	0.02192	ms
GPU sort == 3000 =	0.388384	ms
D2H  == 3000 =	0.024864	ms
GPU full measure == 3000 =	0.892256	ms
CPU malloc == 3000  =	0.006547	ms
CPU sort == 3000  =	0.647162	ms
Warm-up =	124.191	ms
Device malloc  == 5000 =	0.333376	ms
H2D  == 5000 =	0.024	ms
GPU sort == 5000 =	0.4064	ms
D2H  == 5000 =	0.02688	ms
GPU full measure == 5000 =	0.858304	ms
CPU malloc == 5000  =	0.010802	ms
CPU sort == 5000  =	1.1182	ms
Warm-up =	123.007	ms
Device malloc  == 7000 =	0.327296	ms
H2D  == 7000 =	0.02624	ms
GPU sort == 7000 =	0.405088	ms
D2H  == 7000 =	0.029664	ms
GPU full measure == 7000 =	0.854656	ms
CPU malloc == 7000  =	0.032621	ms
CPU sort == 7000  =	1.60337	ms
Warm-up =	123.236	ms
Device malloc  == 10000 =	0.314048	ms
H2D  == 10000 =	0.033152	ms
GPU sort == 10000 =	0.39088	ms
D2H  == 10000 =	0.033312	ms
GPU full measure == 10000 =	0.836896	ms
CPU malloc == 10000  =	0.04672	ms
CPU sort == 10000  =	2.3853	ms
Warm-up =	121.302	ms
Device malloc  == 50000 =	0.210592	ms
H2D  == 50000 =	0.071456	ms
GPU sort == 50000 =	0.428064	ms
D2H  == 50000 =	0.08784	ms
GPU full measure == 50000 =	0.87008	ms
CPU malloc == 50000  =	0.186766	ms
CPU sort == 50000  =	13.9315	ms
Warm-up =	121.606	ms
Device malloc  == 100000 =	0.2736	ms
H2D  == 100000 =	0.126656	ms
GPU sort == 100000 =	0.452928	ms
D2H  == 100000 =	0.152448	ms
GPU full measure == 100000 =	1.07606	ms
CPU malloc == 100000  =	0.370177	ms
CPU sort == 100000  =	29.3491	ms
Warm-up =	123.089	ms
Device malloc  == 300000 =	0.249632	ms
H2D  == 300000 =	0.396512	ms
GPU sort == 300000 =	0.951808	ms
D2H  == 300000 =	0.398272	ms
GPU full measure == 300000 =	2.08131	ms
CPU malloc == 300000  =	1.12019	ms
CPU sort == 300000  =	95.2767	ms
Warm-up =	123.194	ms
Device malloc  == 500000 =	0.229856	ms
H2D  == 500000 =	0.547104	ms
GPU sort == 500000 =	1.24413	ms
D2H  == 500000 =	0.542784	ms
GPU full measure == 500000 =	2.64762	ms
CPU malloc == 500000  =	1.8603	ms
CPU sort == 500000  =	165.784	ms
Warm-up =	123.441	ms
Device malloc  == 700000 =	0.268096	ms
H2D  == 700000 =	0.72736	ms
GPU sort == 700000 =	1.51558	ms
D2H  == 700000 =	0.694176	ms
GPU full measure == 700000 =	3.29146	ms
CPU malloc == 700000  =	2.64969	ms
CPU sort == 700000  =	237.197	ms
Warm-up =	123.605	ms
Device malloc  == 1000000 =	0.22112	ms
H2D  == 1000000 =	0.979168	ms
GPU sort == 1000000 =	1.7823	ms
D2H  == 1000000 =	0.952832	ms
GPU full measure == 1000000 =	4.02506	ms
CPU malloc == 1000000  =	3.70454	ms
CPU sort == 1000000  =	349.319	ms
Warm-up =	122.171	ms
Device malloc  == 5000000 =	0.341856	ms
H2D  == 5000000 =	4.98461	ms
GPU sort == 5000000 =	6.4417	ms
D2H  == 5000000 =	3.24627	ms
GPU full measure == 5000000 =	15.1291	ms
CPU malloc == 5000000  =	18.5437	ms
CPU sort == 5000000  =	1937.9	ms
Warm-up =	141.493	ms
Device malloc  == 10000000 =	1.35014	ms
H2D  == 10000000 =	11.7423	ms
GPU sort == 10000000 =	11.6852	ms
D2H  == 10000000 =	6.21043	ms
GPU full measure == 10000000 =	31.1204	ms
CPU malloc == 10000000  =	36.736	ms
CPU sort == 10000000  =	4031.82	ms
Warm-up =	141.872	ms
Device malloc  == 30000000 =	1.89142	ms
H2D  == 30000000 =	36.4069	ms
GPU sort == 30000000 =	33.1148	ms
D2H  == 30000000 =	44.2499	ms
GPU full measure == 30000000 =	115.816	ms
CPU malloc == 30000000  =	108.961	ms
CPU sort == 30000000  =	12933	ms
Warm-up =	139.851	ms
Device malloc  == 50000000 =	2.48579	ms
H2D  == 50000000 =	73.1172	ms
GPU sort == 50000000 =	53.3824	ms
D2H  == 50000000 =	73.3383	ms
GPU full measure == 50000000 =	202.484	ms
CPU malloc == 50000000  =	184.398	ms
CPU sort == 50000000  =	22118.7	ms
Warm-up =	135.808	ms
Device malloc  == 70000000 =	2.79226	ms
H2D  == 70000000 =	111	ms
GPU sort == 70000000 =	74.7292	ms
D2H  == 70000000 =	101.961	ms
GPU full measure == 70000000 =	290.65	ms
CPU malloc == 70000000  =	267.158	ms
CPU sort == 70000000  =	31659.1	ms
Warm-up =	143.109	ms
Device malloc  == 100000000 =	3.72022	ms
H2D  == 100000000 =	143.285	ms
GPU sort == 100000000 =	106.121	ms
D2H  == 100000000 =	145.878	ms
GPU full measure == 100000000 =	399.169	ms
CPU malloc == 100000000  =	364.722	ms
CPU sort == 100000000  =	45960.9	ms
Warm-up =	138.573	ms
Device malloc  == 1 =	0.717824	ms
H2D  == 1 =	0.015968	ms
GPU sort == 1 =	0.090688	ms
D2H  == 1 =	0.021024	ms
GPU full measure == 1 =	0.915264	ms
CPU malloc == 1  =	0.000538	ms
CPU sort == 1  =	0.000432	ms
Warm-up =	125.512	ms
Device malloc  == 10 =	0.81408	ms
H2D  == 10 =	0.018144	ms
GPU sort == 10 =	0.08784	ms
D2H  == 10 =	0.019296	ms
GPU full measure == 10 =	1.00336	ms
CPU malloc == 10  =	0.000201	ms
CPU sort == 10  =	0.002106	ms
Warm-up =	124.283	ms
Device malloc  == 50 =	0.336352	ms
H2D  == 50 =	0.01632	ms
GPU sort == 50 =	0.092	ms
D2H  == 50 =	0.02112	ms
GPU full measure == 50 =	0.531136	ms
CPU malloc == 50  =	0.000603	ms
CPU sort == 50  =	0.007153	ms
Warm-up =	121.851	ms
Device malloc  == 100 =	0.338432	ms
H2D  == 100 =	0.017216	ms
GPU sort == 100 =	0.093088	ms
D2H  == 100 =	0.018048	ms
GPU full measure == 100 =	0.533344	ms
CPU malloc == 100  =	0.000641	ms
CPU sort == 100  =	0.014983	ms
Warm-up =	122.286	ms
Device malloc  == 200 =	0.317824	ms
H2D  == 200 =	0.01696	ms
GPU sort == 200 =	0.09536	ms
D2H  == 200 =	0.020832	ms
GPU full measure == 200 =	0.517824	ms
CPU malloc == 200  =	0.000823	ms
CPU sort == 200  =	0.031177	ms
Warm-up =	122.449	ms
Device malloc  == 300 =	0.408992	ms
H2D  == 300 =	0.017536	ms
GPU sort == 300 =	0.119808	ms
D2H  == 300 =	0.022528	ms
GPU full measure == 300 =	0.6392	ms
CPU malloc == 300  =	0.001336	ms
CPU sort == 300  =	0.053886	ms
Warm-up =	121.155	ms
Device malloc  == 400 =	0.398144	ms
H2D  == 400 =	0.017408	ms
GPU sort == 400 =	0.092576	ms
D2H  == 400 =	0.021472	ms
GPU full measure == 400 =	0.60624	ms
CPU malloc == 400  =	0.00151	ms
CPU sort == 400  =	0.086009	ms
Warm-up =	122.677	ms
Device malloc  == 500 =	0.374912	ms
H2D  == 500 =	0.017696	ms
GPU sort == 500 =	0.092928	ms
D2H  == 500 =	0.0216	ms
GPU full measure == 500 =	0.573568	ms
CPU malloc == 500  =	0.001669	ms
CPU sort == 500  =	0.086496	ms
Warm-up =	123.68	ms
Device malloc  == 600 =	0.333856	ms
H2D  == 600 =	0.017248	ms
GPU sort == 600 =	0.093696	ms
D2H  == 600 =	0.022368	ms
GPU full measure == 600 =	0.53248	ms
CPU malloc == 600  =	0.001905	ms
CPU sort == 600  =	0.105672	ms
Warm-up =	122.279	ms
Device malloc  == 700 =	0.345344	ms
H2D  == 700 =	0.017728	ms
GPU sort == 700 =	0.093984	ms
D2H  == 700 =	0.021504	ms
GPU full measure == 700 =	0.544992	ms
CPU malloc == 700  =	0.002012	ms
CPU sort == 700  =	0.12527	ms
Warm-up =	123.341	ms
Device malloc  == 800 =	0.336352	ms
H2D  == 800 =	0.01744	ms
GPU sort == 800 =	0.112256	ms
D2H  == 800 =	0.020736	ms
GPU full measure == 800 =	0.55328	ms
CPU malloc == 800  =	0.002248	ms
CPU sort == 800  =	0.153572	ms
Warm-up =	122.306	ms
Device malloc  == 900 =	0.334784	ms
H2D  == 900 =	0.01664	ms
GPU sort == 900 =	0.090848	ms
D2H  == 900 =	0.021888	ms
GPU full measure == 900 =	0.529888	ms
CPU malloc == 900  =	0.002663	ms
CPU sort == 900  =	0.167957	ms
Warm-up =	122.04	ms
Device malloc  == 1000 =	0.341312	ms
H2D  == 1000 =	0.01792	ms
GPU sort == 1000 =	0.098208	ms
D2H  == 1000 =	0.022016	ms
GPU full measure == 1000 =	0.558592	ms
CPU malloc == 1000  =	0.002767	ms
CPU sort == 1000  =	0.187007	ms
Warm-up =	118.598	ms
Device malloc  == 3000 =	0.320896	ms
H2D  == 3000 =	0.021216	ms
GPU sort == 3000 =	0.384608	ms
D2H  == 3000 =	0.02464	ms
GPU full measure == 3000 =	0.81808	ms
CPU malloc == 3000  =	0.00651	ms
CPU sort == 3000  =	0.635505	ms
Warm-up =	124.39	ms
Device malloc  == 5000 =	0.348448	ms
H2D  == 5000 =	0.023808	ms
GPU sort == 5000 =	0.407488	ms
D2H  == 5000 =	0.027552	ms
GPU full measure == 5000 =	0.8752	ms
CPU malloc == 5000  =	0.01078	ms
CPU sort == 5000  =	1.11238	ms
Warm-up =	121.321	ms
Device malloc  == 7000 =	0.314528	ms
H2D  == 7000 =	0.027264	ms
GPU sort == 7000 =	0.39792	ms
D2H  == 7000 =	0.030176	ms
GPU full measure == 7000 =	0.837568	ms
CPU malloc == 7000  =	0.026935	ms
CPU sort == 7000  =	1.6134	ms
Warm-up =	122.535	ms
Device malloc  == 10000 =	0.338624	ms
H2D  == 10000 =	0.029824	ms
GPU sort == 10000 =	0.388896	ms
D2H  == 10000 =	0.033184	ms
GPU full measure == 10000 =	0.857728	ms
CPU malloc == 10000  =	0.039692	ms
CPU sort == 10000  =	2.42757	ms
Warm-up =	122.258	ms
Device malloc  == 50000 =	0.174208	ms
H2D  == 50000 =	0.072352	ms
GPU sort == 50000 =	0.440992	ms
D2H  == 50000 =	0.086048	ms
GPU full measure == 50000 =	0.856224	ms
CPU malloc == 50000  =	0.197191	ms
CPU sort == 50000  =	13.8531	ms
Warm-up =	122.807	ms
Device malloc  == 100000 =	0.302656	ms
H2D  == 100000 =	0.128192	ms
GPU sort == 100000 =	0.447808	ms
D2H  == 100000 =	0.152896	ms
GPU full measure == 100000 =	1.10534	ms
CPU malloc == 100000  =	0.380207	ms
CPU sort == 100000  =	29.3432	ms
Warm-up =	123.423	ms
Device malloc  == 300000 =	0.183776	ms
H2D  == 300000 =	0.4	ms
GPU sort == 300000 =	0.967456	ms
D2H  == 300000 =	0.395488	ms
GPU full measure == 300000 =	2.02592	ms
CPU malloc == 300000  =	1.12168	ms
CPU sort == 300000  =	95.3235	ms
Warm-up =	123.497	ms
Device malloc  == 500000 =	0.280512	ms
H2D  == 500000 =	0.54192	ms
GPU sort == 500000 =	1.23101	ms
D2H  == 500000 =	0.548608	ms
GPU full measure == 500000 =	2.68294	ms
CPU malloc == 500000  =	1.90925	ms
CPU sort == 500000  =	166.75	ms
Warm-up =	123.439	ms
Device malloc  == 700000 =	0.258816	ms
H2D  == 700000 =	0.727712	ms
GPU sort == 700000 =	1.48221	ms
D2H  == 700000 =	0.70336	ms
GPU full measure == 700000 =	3.25798	ms
CPU malloc == 700000  =	2.60798	ms
CPU sort == 700000  =	237.729	ms
Warm-up =	124.202	ms
Device malloc  == 1000000 =	0.211744	ms
H2D  == 1000000 =	0.985184	ms
GPU sort == 1000000 =	2.05165	ms
D2H  == 1000000 =	0.953824	ms
GPU full measure == 1000000 =	4.30307	ms
CPU malloc == 1000000  =	3.75243	ms
CPU sort == 1000000  =	348.898	ms
Warm-up =	123.261	ms
Device malloc  == 5000000 =	0.417888	ms
H2D  == 5000000 =	5.07453	ms
GPU sort == 5000000 =	6.53712	ms
D2H  == 5000000 =	3.23952	ms
GPU full measure == 5000000 =	15.3866	ms
CPU malloc == 5000000  =	18.6319	ms
CPU sort == 5000000  =	1937.36	ms
Warm-up =	141.363	ms
Device malloc  == 10000000 =	0.82592	ms
H2D  == 10000000 =	11.7465	ms
GPU sort == 10000000 =	11.7772	ms
D2H  == 10000000 =	6.20752	ms
GPU full measure == 10000000 =	30.6874	ms
CPU malloc == 10000000  =	37.7198	ms
CPU sort == 10000000  =	4030.66	ms
Warm-up =	137.152	ms
Device malloc  == 30000000 =	1.88909	ms
H2D  == 30000000 =	36.4012	ms
GPU sort == 30000000 =	32.8834	ms
D2H  == 30000000 =	44.2061	ms
GPU full measure == 30000000 =	115.528	ms
CPU malloc == 30000000  =	110.478	ms
CPU sort == 30000000  =	12952.4	ms
Warm-up =	142.506	ms
Device malloc  == 50000000 =	2.44259	ms
H2D  == 50000000 =	72.9565	ms
GPU sort == 50000000 =	53.8495	ms
D2H  == 50000000 =	72.9332	ms
GPU full measure == 50000000 =	202.339	ms
CPU malloc == 50000000  =	183.422	ms
CPU sort == 50000000  =	22175.4	ms
Warm-up =	136.352	ms
Device malloc  == 70000000 =	2.99878	ms
H2D  == 70000000 =	110.396	ms
GPU sort == 70000000 =	74.54	ms
D2H  == 70000000 =	102.004	ms
GPU full measure == 70000000 =	290.091	ms
CPU malloc == 70000000  =	268.399	ms
CPU sort == 70000000  =	31665.1	ms
Warm-up =	137.55	ms
Device malloc  == 100000000 =	3.7111	ms
H2D  == 100000000 =	144.189	ms
GPU sort == 100000000 =	106.017	ms
D2H  == 100000000 =	145.885	ms
GPU full measure == 100000000 =	399.962	ms
CPU malloc == 100000000  =	369.922	ms
CPU sort == 100000000  =	46089.7	ms
Warm-up =	143.955	ms
Device malloc  == 1 =	0.190112	ms
H2D  == 1 =	0.019424	ms
GPU sort == 1 =	0.097408	ms
D2H  == 1 =	0.02368	ms
GPU full measure == 1 =	0.41392	ms
CPU malloc == 1  =	0.000587	ms
CPU sort == 1  =	0.000513	ms
Warm-up =	125.811	ms
Device malloc  == 10 =	1.09987	ms
H2D  == 10 =	0.017472	ms
GPU sort == 10 =	0.095776	ms
D2H  == 10 =	0.019808	ms
GPU full measure == 10 =	1.3129	ms
CPU malloc == 10  =	0.00024	ms
CPU sort == 10  =	0.002263	ms
Warm-up =	124.109	ms
Device malloc  == 50 =	0.699392	ms
H2D  == 50 =	0.016096	ms
GPU sort == 50 =	0.092928	ms
D2H  == 50 =	0.02064	ms
GPU full measure == 50 =	0.895744	ms
CPU malloc == 50  =	0.000606	ms
CPU sort == 50  =	0.007303	ms
Warm-up =	125.182	ms
Device malloc  == 100 =	0.817248	ms
H2D  == 100 =	0.017824	ms
GPU sort == 100 =	0.093312	ms
D2H  == 100 =	0.01936	ms
GPU full measure == 100 =	1.01146	ms
CPU malloc == 100  =	0.000731	ms
CPU sort == 100  =	0.014657	ms
Warm-up =	124.936	ms
Device malloc  == 200 =	0.80032	ms
H2D  == 200 =	0.018016	ms
GPU sort == 200 =	0.088096	ms
D2H  == 200 =	0.019488	ms
GPU full measure == 200 =	0.992608	ms
CPU malloc == 200  =	0.000859	ms
CPU sort == 200  =	0.030984	ms
Warm-up =	125.025	ms
Device malloc  == 300 =	0.32208	ms
H2D  == 300 =	0.017024	ms
GPU sort == 300 =	0.088032	ms
D2H  == 300 =	0.02096	ms
GPU full measure == 300 =	0.515456	ms
CPU malloc == 300  =	0.001328	ms
CPU sort == 300  =	0.049818	ms
Warm-up =	123.6	ms
Device malloc  == 400 =	0.314816	ms
H2D  == 400 =	0.016928	ms
GPU sort == 400 =	0.09712	ms
D2H  == 400 =	0.021376	ms
GPU full measure == 400 =	0.517568	ms
CPU malloc == 400  =	0.001435	ms
CPU sort == 400  =	0.066453	ms
Warm-up =	123.33	ms
Device malloc  == 500 =	0.363936	ms
H2D  == 500 =	0.017664	ms
GPU sort == 500 =	0.095232	ms
D2H  == 500 =	0.020576	ms
GPU full measure == 500 =	0.575648	ms
CPU malloc == 500  =	0.001729	ms
CPU sort == 500  =	0.086311	ms
Warm-up =	121.995	ms
Device malloc  == 600 =	0.382144	ms
H2D  == 600 =	0.016096	ms
GPU sort == 600 =	0.111808	ms
D2H  == 600 =	0.022048	ms
GPU full measure == 600 =	0.600576	ms
CPU malloc == 600  =	0.001811	ms
CPU sort == 600  =	0.105552	ms
Warm-up =	122.817	ms
Device malloc  == 700 =	0.377504	ms
H2D  == 700 =	0.016992	ms
GPU sort == 700 =	0.092576	ms
D2H  == 700 =	0.01872	ms
GPU full measure == 700 =	0.582112	ms
CPU malloc == 700  =	0.002078	ms
CPU sort == 700  =	0.124989	ms
Warm-up =	122.975	ms
Device malloc  == 800 =	0.321824	ms
H2D  == 800 =	0.017376	ms
GPU sort == 800 =	0.095808	ms
D2H  == 800 =	0.02176	ms
GPU full measure == 800 =	0.522656	ms
CPU malloc == 800  =	0.002288	ms
CPU sort == 800  =	0.145429	ms
Warm-up =	122.458	ms
Device malloc  == 900 =	0.341088	ms
H2D  == 900 =	0.017152	ms
GPU sort == 900 =	0.09408	ms
D2H  == 900 =	0.021632	ms
GPU full measure == 900 =	0.540544	ms
CPU malloc == 900  =	0.002457	ms
CPU sort == 900  =	0.169506	ms
Warm-up =	124.313	ms
Device malloc  == 1000 =	0.480448	ms
H2D  == 1000 =	0.017152	ms
GPU sort == 1000 =	0.095808	ms
D2H  == 1000 =	0.022208	ms
GPU full measure == 1000 =	0.68432	ms
CPU malloc == 1000  =	0.002781	ms
CPU sort == 1000  =	0.202006	ms
Warm-up =	123.477	ms
Device malloc  == 3000 =	0.365824	ms
H2D  == 3000 =	0.021696	ms
GPU sort == 3000 =	0.392096	ms
D2H  == 3000 =	0.024928	ms
GPU full measure == 3000 =	0.87312	ms
CPU malloc == 3000  =	0.006541	ms
CPU sort == 3000  =	0.632789	ms
Warm-up =	123.701	ms
Device malloc  == 5000 =	0.427584	ms
H2D  == 5000 =	0.023712	ms
GPU sort == 5000 =	0.392128	ms
D2H  == 5000 =	0.026432	ms
GPU full measure == 5000 =	0.936512	ms
CPU malloc == 5000  =	0.010539	ms
CPU sort == 5000  =	1.11672	ms
Warm-up =	118.499	ms
Device malloc  == 7000 =	0.348288	ms
H2D  == 7000 =	0.026528	ms
GPU sort == 7000 =	0.40176	ms
D2H  == 7000 =	0.029984	ms
GPU full measure == 7000 =	0.875424	ms
CPU malloc == 7000  =	0.026956	ms
CPU sort == 7000  =	1.60925	ms
Warm-up =	123.237	ms
Device malloc  == 10000 =	0.321056	ms
H2D  == 10000 =	0.030912	ms
GPU sort == 10000 =	0.4	ms
D2H  == 10000 =	0.03392	ms
GPU full measure == 10000 =	0.853472	ms
CPU malloc == 10000  =	0.046072	ms
CPU sort == 10000  =	2.38867	ms
Warm-up =	120.289	ms
Device malloc  == 50000 =	0.206304	ms
H2D  == 50000 =	0.07312	ms
GPU sort == 50000 =	0.412032	ms
D2H  == 50000 =	0.085344	ms
GPU full measure == 50000 =	0.8448	ms
CPU malloc == 50000  =	0.19594	ms
CPU sort == 50000  =	13.9039	ms
Warm-up =	141.901	ms
Device malloc  == 100000 =	0.211712	ms
H2D  == 100000 =	0.13904	ms
GPU sort == 100000 =	0.463136	ms
D2H  == 100000 =	0.179264	ms
GPU full measure == 100000 =	1.06835	ms
CPU malloc == 100000  =	0.389143	ms
CPU sort == 100000  =	29.3895	ms
Warm-up =	146.532	ms
Device malloc  == 300000 =	0.208224	ms
H2D  == 300000 =	0.455872	ms
GPU sort == 300000 =	1.01408	ms
D2H  == 300000 =	0.47168	ms
GPU full measure == 300000 =	2.24442	ms
CPU malloc == 300000  =	1.45477	ms
CPU sort == 300000  =	105.542	ms
Warm-up =	120.561	ms
Device malloc  == 500000 =	0.190368	ms
H2D  == 500000 =	0.544384	ms
GPU sort == 500000 =	1.24589	ms
D2H  == 500000 =	0.55088	ms
GPU full measure == 500000 =	2.61462	ms
CPU malloc == 500000  =	1.86966	ms
CPU sort == 500000  =	165.613	ms
Warm-up =	143.191	ms
Device malloc  == 700000 =	0.190336	ms
H2D  == 700000 =	0.755008	ms
GPU sort == 700000 =	1.48848	ms
D2H  == 700000 =	0.752032	ms
GPU full measure == 700000 =	3.2712	ms
CPU malloc == 700000  =	2.64352	ms
CPU sort == 700000  =	237.784	ms
Warm-up =	121.458	ms
Device malloc  == 1000000 =	0.206624	ms
H2D  == 1000000 =	0.965504	ms
GPU sort == 1000000 =	2.15466	ms
D2H  == 1000000 =	0.9432	ms
GPU full measure == 1000000 =	4.35747	ms
CPU malloc == 1000000  =	3.78474	ms
CPU sort == 1000000  =	348.483	ms
Warm-up =	118.738	ms
Device malloc  == 5000000 =	0.369408	ms
H2D  == 5000000 =	4.9927	ms
GPU sort == 5000000 =	6.61277	ms
D2H  == 5000000 =	3.24458	ms
GPU full measure == 5000000 =	15.3422	ms
CPU malloc == 5000000  =	18.7766	ms
CPU sort == 5000000  =	1943.22	ms
Warm-up =	143.644	ms
Device malloc  == 10000000 =	0.825792	ms
H2D  == 10000000 =	11.7757	ms
GPU sort == 10000000 =	11.7129	ms
D2H  == 10000000 =	6.24288	ms
GPU full measure == 10000000 =	30.6926	ms
CPU malloc == 10000000  =	36.6138	ms
CPU sort == 10000000  =	4029.1	ms
Warm-up =	138.398	ms
Device malloc  == 30000000 =	1.89568	ms
H2D  == 30000000 =	36.4087	ms
GPU sort == 30000000 =	32.7814	ms
D2H  == 30000000 =	44.1815	ms
GPU full measure == 30000000 =	115.433	ms
CPU malloc == 30000000  =	110.741	ms
CPU sort == 30000000  =	12952.4	ms
Warm-up =	141.919	ms
Device malloc  == 50000000 =	2.48102	ms
H2D  == 50000000 =	72.9637	ms
GPU sort == 50000000 =	53.8815	ms
D2H  == 50000000 =	72.5906	ms
GPU full measure == 50000000 =	202.077	ms
CPU malloc == 50000000  =	182.003	ms
CPU sort == 50000000  =	22146.6	ms
Warm-up =	141.801	ms
Device malloc  == 70000000 =	2.98112	ms
H2D  == 70000000 =	110.868	ms
GPU sort == 70000000 =	74.5116	ms
D2H  == 70000000 =	102.744	ms
GPU full measure == 70000000 =	291.263	ms
CPU malloc == 70000000  =	264.184	ms
CPU sort == 70000000  =	31658.8	ms
Warm-up =	119.2	ms
Device malloc  == 100000000 =	3.71974	ms
H2D  == 100000000 =	143.569	ms
GPU sort == 100000000 =	106.402	ms
D2H  == 100000000 =	145.456	ms
GPU full measure == 100000000 =	399.309	ms
CPU malloc == 100000000  =	366.034	ms
CPU sort == 100000000  =	46031.6	ms
Warm-up =	143.149	ms
Device malloc  == 1 =	0.839648	ms
H2D  == 1 =	0.016864	ms
GPU sort == 1 =	0.091328	ms
D2H  == 1 =	0.020672	ms
GPU full measure == 1 =	1.03645	ms
CPU malloc == 1  =	0.0005	ms
CPU sort == 1  =	0.00067	ms
Warm-up =	126.199	ms
Device malloc  == 10 =	0.722112	ms
H2D  == 10 =	0.017408	ms
GPU sort == 10 =	0.086272	ms
D2H  == 10 =	0.020832	ms
GPU full measure == 10 =	0.9136	ms
CPU malloc == 10  =	0.0002	ms
CPU sort == 10  =	0.001838	ms
Warm-up =	118.521	ms
Device malloc  == 50 =	0.695552	ms
H2D  == 50 =	0.016096	ms
GPU sort == 50 =	0.094464	ms
D2H  == 50 =	0.020736	ms
GPU full measure == 50 =	0.89168	ms
CPU malloc == 50  =	0.000621	ms
CPU sort == 50  =	0.007491	ms
Warm-up =	123.098	ms
Device malloc  == 100 =	0.387776	ms
H2D  == 100 =	0.017184	ms
GPU sort == 100 =	0.087936	ms
D2H  == 100 =	0.01984	ms
GPU full measure == 100 =	0.577984	ms
CPU malloc == 100  =	0.000725	ms
CPU sort == 100  =	0.014644	ms
Warm-up =	120.2	ms
Device malloc  == 200 =	0.379808	ms
H2D  == 200 =	0.01744	ms
GPU sort == 200 =	0.08656	ms
D2H  == 200 =	0.020288	ms
GPU full measure == 200 =	0.5712	ms
CPU malloc == 200  =	0.000881	ms
CPU sort == 200  =	0.031228	ms
Warm-up =	118.207	ms
Device malloc  == 300 =	0.318144	ms
H2D  == 300 =	0.016864	ms
GPU sort == 300 =	0.089376	ms
D2H  == 300 =	0.0208	ms
GPU full measure == 300 =	0.510848	ms
CPU malloc == 300  =	0.001229	ms
CPU sort == 300  =	0.049373	ms
Warm-up =	124.461	ms
Device malloc  == 400 =	0.322304	ms
H2D  == 400 =	0.016384	ms
GPU sort == 400 =	0.095072	ms
D2H  == 400 =	0.021408	ms
GPU full measure == 400 =	0.522208	ms
CPU malloc == 400  =	0.001344	ms
CPU sort == 400  =	0.066616	ms
Warm-up =	123.193	ms
Device malloc  == 500 =	0.318496	ms
H2D  == 500 =	0.017504	ms
GPU sort == 500 =	0.09792	ms
D2H  == 500 =	0.02144	ms
GPU full measure == 500 =	0.52208	ms
CPU malloc == 500  =	0.001764	ms
CPU sort == 500  =	0.08614	ms
Warm-up =	118.188	ms
Device malloc  == 600 =	0.311808	ms
H2D  == 600 =	0.017856	ms
GPU sort == 600 =	0.094464	ms
D2H  == 600 =	0.021088	ms
GPU full measure == 600 =	0.51056	ms
CPU malloc == 600  =	0.001828	ms
CPU sort == 600  =	0.105464	ms
Warm-up =	123.056	ms
Device malloc  == 700 =	0.332896	ms
H2D  == 700 =	0.017952	ms
GPU sort == 700 =	0.09008	ms
D2H  == 700 =	0.021952	ms
GPU full measure == 700 =	0.530336	ms
CPU malloc == 700  =	0.002044	ms
CPU sort == 700  =	0.125446	ms
Warm-up =	120.244	ms
Device malloc  == 800 =	0.36928	ms
H2D  == 800 =	0.017728	ms
GPU sort == 800 =	0.087648	ms
D2H  == 800 =	0.019648	ms
GPU full measure == 800 =	0.559776	ms
CPU malloc == 800  =	0.002108	ms
CPU sort == 800  =	0.146675	ms
Warm-up =	122.288	ms
Device malloc  == 900 =	0.315936	ms
H2D  == 900 =	0.018336	ms
GPU sort == 900 =	0.094912	ms
D2H  == 900 =	0.021664	ms
GPU full measure == 900 =	0.518912	ms
CPU malloc == 900  =	0.002501	ms
CPU sort == 900  =	0.168043	ms
Warm-up =	137.804	ms
Device malloc  == 1000 =	0.32704	ms
H2D  == 1000 =	0.018304	ms
GPU sort == 1000 =	0.09184	ms
D2H  == 1000 =	0.022752	ms
GPU full measure == 1000 =	0.540192	ms
CPU malloc == 1000  =	0.002698	ms
CPU sort == 1000  =	0.187817	ms
Warm-up =	122.494	ms
Device malloc  == 3000 =	0.36976	ms
H2D  == 3000 =	0.020768	ms
GPU sort == 3000 =	0.396672	ms
D2H  == 3000 =	0.024608	ms
GPU full measure == 3000 =	0.892928	ms
CPU malloc == 3000  =	0.006913	ms
CPU sort == 3000  =	0.633281	ms
Warm-up =	118.287	ms
Device malloc  == 5000 =	0.333184	ms
H2D  == 5000 =	0.023648	ms
GPU sort == 5000 =	0.3976	ms
D2H  == 5000 =	0.02688	ms
GPU full measure == 5000 =	0.84896	ms
CPU malloc == 5000  =	0.010577	ms
CPU sort == 5000  =	1.11079	ms
Warm-up =	121.908	ms
Device malloc  == 7000 =	0.335392	ms
H2D  == 7000 =	0.024672	ms
GPU sort == 7000 =	0.400096	ms
D2H  == 7000 =	0.028864	ms
GPU full measure == 7000 =	0.855232	ms
CPU malloc == 7000  =	0.026978	ms
CPU sort == 7000  =	1.6081	ms
Warm-up =	121.75	ms
Device malloc  == 10000 =	0.334144	ms
H2D  == 10000 =	0.030496	ms
GPU sort == 10000 =	0.388224	ms
D2H  == 10000 =	0.034048	ms
GPU full measure == 10000 =	0.85504	ms
CPU malloc == 10000  =	0.045864	ms
CPU sort == 10000  =	2.3996	ms
Warm-up =	117.942	ms
Device malloc  == 50000 =	0.167488	ms
H2D  == 50000 =	0.07184	ms
GPU sort == 50000 =	0.431392	ms
D2H  == 50000 =	0.087296	ms
GPU full measure == 50000 =	0.829792	ms
CPU malloc == 50000  =	0.197476	ms
CPU sort == 50000  =	13.8431	ms
Warm-up =	144.022	ms
Device malloc  == 100000 =	0.309344	ms
H2D  == 100000 =	0.143648	ms
GPU sort == 100000 =	0.460768	ms
D2H  == 100000 =	0.178656	ms
GPU full measure == 100000 =	1.17718	ms
CPU malloc == 100000  =	0.487043	ms
CPU sort == 100000  =	35.9634	ms
Warm-up =	123.665	ms
Device malloc  == 300000 =	0.188224	ms
H2D  == 300000 =	0.398432	ms
GPU sort == 300000 =	0.957664	ms
D2H  == 300000 =	0.398752	ms
GPU full measure == 300000 =	2.02118	ms
CPU malloc == 300000  =	1.12302	ms
CPU sort == 300000  =	95.1054	ms
Warm-up =	122.076	ms
Device malloc  == 500000 =	0.192256	ms
H2D  == 500000 =	0.544544	ms
GPU sort == 500000 =	1.24621	ms
D2H  == 500000 =	0.548064	ms
GPU full measure == 500000 =	2.61606	ms
CPU malloc == 500000  =	1.86241	ms
CPU sort == 500000  =	165.863	ms
Warm-up =	124.056	ms
Device malloc  == 700000 =	0.184448	ms
H2D  == 700000 =	0.718496	ms
GPU sort == 700000 =	1.5064	ms
D2H  == 700000 =	0.700096	ms
GPU full measure == 700000 =	3.1967	ms
CPU malloc == 700000  =	2.66263	ms
CPU sort == 700000  =	239.481	ms
Warm-up =	123.24	ms
Device malloc  == 1000000 =	0.210336	ms
H2D  == 1000000 =	0.985184	ms
GPU sort == 1000000 =	1.80493	ms
D2H  == 1000000 =	0.953888	ms
GPU full measure == 1000000 =	4.04022	ms
CPU malloc == 1000000  =	3.85736	ms
CPU sort == 1000000  =	351.085	ms
Warm-up =	121.892	ms
Device malloc  == 5000000 =	0.418944	ms
H2D  == 5000000 =	5.08346	ms
GPU sort == 5000000 =	6.55424	ms
D2H  == 5000000 =	3.25581	ms
GPU full measure == 5000000 =	15.4291	ms
CPU malloc == 5000000  =	19.1653	ms
CPU sort == 5000000  =	1942.51	ms
Warm-up =	139.426	ms
Device malloc  == 10000000 =	0.813632	ms
H2D  == 10000000 =	11.7603	ms
GPU sort == 10000000 =	11.7014	ms
D2H  == 10000000 =	6.2071	ms
GPU full measure == 10000000 =	30.6141	ms
CPU malloc == 10000000  =	37.1033	ms
CPU sort == 10000000  =	4036.74	ms
Warm-up =	142.522	ms
Device malloc  == 30000000 =	1.91594	ms
H2D  == 30000000 =	35.9122	ms
GPU sort == 30000000 =	32.9937	ms
D2H  == 30000000 =	44.1732	ms
GPU full measure == 30000000 =	115.143	ms
CPU malloc == 30000000  =	112.081	ms
CPU sort == 30000000  =	12966.3	ms
Warm-up =	144.131	ms
Device malloc  == 50000000 =	2.21389	ms
H2D  == 50000000 =	72.6281	ms
GPU sort == 50000000 =	53.8689	ms
D2H  == 50000000 =	72.8756	ms
GPU full measure == 50000000 =	201.746	ms
CPU malloc == 50000000  =	192.758	ms
CPU sort == 50000000  =	22142.4	ms
Warm-up =	139.908	ms
Device malloc  == 70000000 =	2.84979	ms
H2D  == 70000000 =	111.283	ms
GPU sort == 70000000 =	74.7715	ms
D2H  == 70000000 =	102.461	ms
GPU full measure == 70000000 =	291.528	ms
CPU malloc == 70000000  =	258.473	ms
CPU sort == 70000000  =	31604.8	ms
Warm-up =	141.321	ms
Device malloc  == 100000000 =	3.29091	ms
H2D  == 100000000 =	144.228	ms
GPU sort == 100000000 =	105.462	ms
D2H  == 100000000 =	146.394	ms
GPU full measure == 100000000 =	399.544	ms
CPU malloc == 100000000  =	364.474	ms
CPU sort == 100000000  =	45997.9	ms
